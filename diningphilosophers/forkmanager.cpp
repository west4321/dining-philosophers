#include "forkmanager.h"
#include "philosopher.h"

#include <QThread>
#include <QTimer>
#include <QDebug>

uint qHash(const ForkManager::Fork &f)
{
	return qHash(qMakePair(f.min, f.max));
}


/**
 * @brief ForkManager::ForkManager
 *
 * ForkManager is responsible for managing philosophers life cycles, synchronization and updating UI state.
 * He does all of this without any single mutex or semaphore.
 * I wanted to write it in the most Qt-ish way I could so I have used Qt Meta Object System mechanisms: signals&slots and method invokation.
 */

ForkManager::ForkManager(QObject *parent) :
	QObject(parent),
	m_philosopherId(1)
{
	qDebug() << Q_FUNC_INFO;
}

ForkManager::~ForkManager()
{
	qDeleteAll(m_philosophers.values());
	m_philosophers.clear();
}

void ForkManager::addPhilosopher(const QString &name)
{
	// Because of fork representation Manager sometimes needs to queue new Philosophers
	if(!canAdd())
	{
		m_philsToAdd.append(name);
		return;
	}
	qDebug() << Q_FUNC_INFO;

	Philosopher *p = new Philosopher(m_philosopherId++, name);
	m_philosophers.insert(p->id(), p);
	emit philosopherAdded(p->id(), p->name(), p->status());

	QThread *th = new QThread(this);
	connect(p, SIGNAL(destroyed(QObject*)), th, SLOT(quit()));
	connect(p, SIGNAL(forksNeeded(int)), this, SLOT(onForksNeeded(int)));
	connect(p, SIGNAL(forksNotNeeded(int)), this, SLOT(onForksNotNeeded(int)));
	connect(p, SIGNAL(statusChanged(int, Philosopher::Status)), this, SLOT(onStatusChanged(int, Philosopher::Status)));

	th->start();
	p->moveToThread(th);
	// calling initialize method in new QThread
	QTimer::singleShot(0, p, SLOT(initialize()));
	QTimer::singleShot(0, this, SLOT(processQueued()));
}

void ForkManager::removePhilosopher(int id)
{
	m_queue.removeOne(id);
	// If fork is in use Philosopher will be removed later
	if (!canRemove(id))
	{
		if (!m_philsToRemove.contains(id))
		{
			m_philsToRemove.insert(id);
		}
		return;
	}
	qDebug() << Q_FUNC_INFO << id;
	QTimer::singleShot(0, m_philosophers.value(id), SLOT(deleteLater()));
	m_philosophers.remove(id);
	emit philosopherRemoved(id);
}

void ForkManager::onForksNeeded(int id)
{
	if (!m_philsToRemove.contains(id))
	{
		if (!processHungryPhilosopher(id))
		{
			// not enough free forks
			m_queue.enqueue(id);
		}
	}
}

void ForkManager::onForksNotNeeded(int id)
{
	qDebug() << Q_FUNC_INFO << id;
	QPair<int, int> nPair = findNeighbours(id);
	if (nPair.first != nPair.second)
	{
		Fork l, r;
		l.min = qMin(nPair.first, id);
		l.max = qMax(nPair.first, id);
		r.min = qMin(nPair.second, id);
		r.max = qMax(nPair.second, id);

		m_takenForks.remove(l);
		m_takenForks.remove(r);
	}
	processToRemove();
	processToAdd();
	processQueued();
}

void ForkManager::onStatusChanged(int id, Philosopher::Status status)
{
	qDebug() << Q_FUNC_INFO << id << status;
	emit philosopherStatusChanged(id, status);
}

void ForkManager::processQueued()
{
	for (int i = 0; i < m_queue.size(); i++)
	{
		if (processHungryPhilosopher(m_queue.at(i)))
		{
			m_queue.removeAt(i);
			i--;
		}
	}
}

bool ForkManager::canAdd()
{
	if (m_philosophers.size() > 0)
	{
		Fork f;
		f.min = m_philosophers.keys().first();
		f.max = m_philosophers.keys().last();
		if (m_takenForks.contains(f))
		{
			return false;
		}
	}
	return true;
}

bool ForkManager::canRemove(int id)
{
	QPair<int, int> pair = findNeighbours(id);
	if (pair.first != pair.second)
	{
		Fork l, r;
		l.min = qMin(pair.first, id);
		l.max = qMax(pair.first, id);
		r.min = qMin(pair.second, id);
		r.max = qMax(pair.second, id);

		if (m_takenForks.contains(l) || m_takenForks.contains(r))
		{
			return false;
		}
	}
	return true;
}

QPair<int, int> ForkManager::findNeighbours(int id) const
{
	auto it = m_philosophers.find(id);
	if (it == m_philosophers.end())
	{
		qCritical() << Q_FUNC_INFO << "Didn't find philosopher";
		return qMakePair(0, 0);
	}
	auto left = it;
	if (it == m_philosophers.begin())
	{
		left = m_philosophers.end() - 1;
	}
	else
	{
		left = it - 1;
	}

	auto right = it + 1;
	if (right == m_philosophers.end())
	{
		right = m_philosophers.begin();
	}

	return qMakePair(left.key(), right.key());
}

bool ForkManager::processHungryPhilosopher(int id)
{
	if (m_philosophers.size() > 2)
	{
		QPair<int, int> nPair = findNeighbours(id);
		if (nPair.first != nPair.second)
		{
			// Philosopher in the neighbourhood wants to leave the table
			if (m_philsToRemove.contains(nPair.first) ||
				m_philsToRemove.contains(nPair.second))
			{
				return false;
			}
			Fork l, r;
			// fork is being described by sorted pair of philosopher ID-s
			l.min = qMin(nPair.first, id);
			l.max = qMax(nPair.first, id);
			r.min = qMin(nPair.second, id);
			r.max = qMax(nPair.second, id);

			if (!m_takenForks.contains(l) && !m_takenForks.contains(r))
			{
				m_takenForks.insert(l);
				m_takenForks.insert(r);
				Philosopher *p = m_philosophers.value(id);
				QMetaObject::invokeMethod(p, "onForksReady");
				return true;
			}
		}
	}
	return false;
}

void ForkManager::processToAdd()
{
	qDebug() << Q_FUNC_INFO << m_philsToAdd;
	if (canAdd())
	{
		foreach (QString name, m_philsToAdd) {
			addPhilosopher(name);
		}
		m_philsToAdd.clear();
	}
}

void ForkManager::processToRemove()
{
	QList<int> toRemove = m_philsToRemove.toList();
	qDebug() << Q_FUNC_INFO << "Before:" << toRemove;
	for (int i = toRemove.size() - 1; i >= 0; i--)
	{
		const int id = toRemove.at(i);
		if (canRemove(id))
		{
			removePhilosopher(id);
			toRemove.removeAt(i);
		}
	}
	m_philsToRemove = toRemove.toSet();
	qDebug() << Q_FUNC_INFO << "After:" << toRemove;
}
