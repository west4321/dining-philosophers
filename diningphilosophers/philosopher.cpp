#include "philosopher.h"

#include <QTimer>
#include <QThread>
#include <QDebug>

const int DEFAULT_THINKING_INTERVAL_SECONDS = 5;
const int DEFAULT_EATING_INTERVAL_SECONDS = 5;

/**
 * @brief Philosopher::Philosopher
 *
 * This class represents Dining Philosopher, it actually does nothing during Eating or Thinking.
 * I have added one state - Waiting. Philosopher in this state is thinking but he is hungry and Fork Manager have to avoid his starvation.
 */

Philosopher::Philosopher(int id, const QString &name, QObject *parent) :
	QObject(parent),
	m_id(id),
	m_name(name),
	m_status(ThinkingStatus),
	m_timer(nullptr)
{
}

void Philosopher::initialize()
{
	if (m_timer != nullptr)
	{
		// initialized already
		qFatal("Philosopher already initialized");
	}
	m_timer = new QTimer(this);
	m_timer->setSingleShot(true);
	connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
	m_timer->start(DEFAULT_THINKING_INTERVAL_SECONDS * 1000);
}

Philosopher::Status Philosopher::status() const
{
	return m_status;
}

int Philosopher::id() const
{
	return m_id;
}

QString Philosopher::name() const
{
	return m_name;
}

void Philosopher::onForksReady()
{
	qDebug() << Q_FUNC_INFO << QThread::currentThreadId();
	setStatus(EatingStatus);
	m_timer->start(DEFAULT_EATING_INTERVAL_SECONDS * 1000);
}

void Philosopher::setStatus(Philosopher::Status s)
{
	if (m_status == s)
		return;

	m_status = s;
	emit statusChanged(id(), status());
}

void Philosopher::onTimeout()
{
	if (m_status == ThinkingStatus)
	{
		setStatus(WaitingStatus);
		emit forksNeeded(id());
	}
	else if (m_status == EatingStatus)
	{
		setStatus(ThinkingStatus);
		m_timer->start(DEFAULT_THINKING_INTERVAL_SECONDS * 1000);
		emit forksNotNeeded(id());
	}
}
