#ifndef PHILOSOPHER_H
#define PHILOSOPHER_H

#include <QObject>

class QTimer;

class Philosopher : public QObject
{
	Q_OBJECT
	Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)
	Q_PROPERTY(int id READ id)

public:
	enum Status {
		ThinkingStatus = 1,
		WaitingStatus,
		EatingStatus
	};

	explicit Philosopher(int id, const QString &name, QObject *parent = nullptr);
	int id() const;
	QString name() const;
	Status status() const;

public slots:
	void initialize();
	void onForksReady();

signals:
	void forksNeeded(int id);
	void forksNotNeeded(int id);
	void statusChanged(int id, Philosopher::Status status);

private slots:
	void onTimeout();
	void setStatus(Status status);

private:
	int m_id;
	QString m_name;
	Status m_status;
	QTimer *m_timer;
};

#endif // PHILOSOPHER_H
