#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <forkmanager.h>

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	qmlRegisterType<ForkManager>("io.qt.examples.forkmanager", 1, 0, "ForkManager");
	qRegisterMetaType<Philosopher::Status>("Philosopher::Status");

	ForkManager fm;
	QQmlApplicationEngine engine;
	engine.rootContext()->setContextProperty("forkManager", &fm);
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

    return app.exec();
}
