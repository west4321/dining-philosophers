#ifndef FORKMANAGER_H
#define FORKMANAGER_H

#include <QObject>
#include <QMap>
#include <QQueue>
#include <QSet>

#include <philosopher.h>

class QThread;

class ForkManager : public QObject
{
	Q_OBJECT
public:
	explicit ForkManager(QObject *parent = nullptr);
	~ForkManager();

	struct Fork {
		int min;
		int max;

		bool operator ==(const Fork &other) const {
			return min == other.min && max == other.max;
		}
	};

public slots:
	void addPhilosopher(const QString &name);
	void removePhilosopher(int id);

private slots:
	void onForksNeeded(int id);
	void onForksNotNeeded(int id);
	void onStatusChanged(int id, Philosopher::Status status);
	void processQueued();

signals:
	void philosopherAdded(int id, QString name, int status);
	void philosopherStatusChanged(int id, int status);
	void philosopherRemoved(int id);


private:
	QPair<int, int> findNeighbours(int id) const;
	bool processHungryPhilosopher(int id);
	void processToAdd();
	void processToRemove();

	bool canAdd();
	bool canRemove(int id);

private:
	int m_philosopherId;
	QMap<int, Philosopher *> m_philosophers;
	QSet<Fork> m_takenForks;
	QQueue<int> m_queue;
	QList<QString> m_philsToAdd;
	QSet<int> m_philsToRemove;
};

#endif // FORKMANAGER_H
