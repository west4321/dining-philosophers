import QtQuick 2.8
import QtQuick.Window 2.2
import io.qt.examples.forkmanager 1.0


Window {
	id: mainWindow;
	visible: true;
	width: 1280;
	height: 720;
	color: "azure"
	property int radiusRatio: 20;

	Component.onCompleted: {
		console.log("Completed");
		for(var i = 0; i < 5; i++) {
			forkManager.addPhilosopher("Phil");
		}
	}

	Connections {
		target: forkManager;

		onPhilosopherAdded: {
			listModel.append({pid: id, name: name, status: status});
		}

		onPhilosopherStatusChanged: {
			for(var i = 0; i < listModel.count; i++) {
				var elemCur = listModel.get(i);
				console.log(elemCur.pid + " " + id);
				if(id == elemCur.pid) {
					elemCur.status = status;
					console.log("status: " + elemCur.status);
					listModel.set(i, elemCur);
					break;
				}
			}
		}

		onPhilosopherRemoved: {
			for(var i = 0; i < listModel.count; i++) {
				var elemCur = listModel.get(i);
				if(id == elemCur.pid) {
					console.log("before remove")
					listModel.remove(i);
					console.log("after remove")
					break;
				}
			}
		}
	}

	ListModel {
		id: listModel;
	}

	Rectangle {
		id: insertWidget;
		property int commonMargin: 20;
		radius: width / mainWindow.radiusRatio;
		anchors.right: parent.right;
		anchors.rightMargin: commonMargin;
		anchors.verticalCenter: parent.verticalCenter;
		height: parent.height / 6;
		width: mainWindow.width - table.width - table.x - 100;
		border.width: 2;
		color: "lightgrey";

		TextEdit {
			id: name;
			property int commonMargin: 8;
			anchors.right: parent.right;
			anchors.rightMargin: commonMargin;
			anchors.top: parent.top;
			anchors.topMargin: commonMargin;
			anchors.left: parent.left;
			anchors.leftMargin: commonMargin;
			anchors.bottom: addButton.top;
			anchors.bottomMargin: commonMargin;

			font.pointSize: height / 2;
			text: "Phil";

		}

		Rectangle {
			id: addButton;
			property int commonMargin: 8;
			radius: width / mainWindow.radiusRatio;
			anchors.right: parent.right;
			anchors.rightMargin: commonMargin;
			anchors.bottom: parent.bottom;
			anchors.bottomMargin: commonMargin;
			anchors.left: parent.left;
			anchors.leftMargin: commonMargin;
			height: parent.height / 3;
			border.width: 2;

			Text {
				id: textLine;
				anchors.centerIn: parent;
				text: "ADD";
				font.pointSize: ((parent.height * 3) < (parent.width * 2) ? parent.height / 2 : parent.width / 3);
			}

			MouseArea {
				anchors.fill: parent;
				hoverEnabled: true;

				onClicked: {
					if (name.text.length > 0)
					{
						forkManager.addPhilosopher(name.text);
					}
				}

				onEntered: {
					parent.color = "lightskyblue";
				}

				onPressed: {
					parent.color = "cadetblue";
				}

				onReleased: {
					console.log(parent.color);
					if (this.containsMouse)
					{
						parent.color = "lightskyblue";
					}
					else
					{
						parent.color = "white";
					}
				}

				onExited: {
					if (!this.pressed)
					{
						parent.color = "white";
					}
				}
			}
		}
	}

	Rectangle {
		id: table;
		x: 80;
		y: 80;
		width: parent.width / 2;
		height: parent.height - 160;
		color: "saddlebrown";
		border.color: "black"
		border.width: 2;

		PathView {
			anchors.fill: parent;
			model: listModel;
			delegate: philosopherDelegate
			path: Path {
				startX: 0;
				startY: table.height / 2;
				PathArc {
					relativeX: table.width;
					relativeY: 0;
					radiusX: table.width * 0.5;
					radiusY: table.height * 0.5;
					useLargeArc: true;
				}
				PathArc {
					relativeX: - table.width;
					relativeY: 0;
					radiusX: table.width * 0.5;
					radiusY: table.height * 0.5;
					useLargeArc: true;
				}
			}

		}

		Component {
			id: philosopherDelegate
			Rectangle {
				width: table.width / 5;
				height: table.height / 5;
				border.width: 2

				Rectangle {
					width: parent.width / 5;
					height: parent.height / 5;
					anchors.right: parent.right;
					anchors.top: parent.top
					border.width: 2;
					color: "red";

					MouseArea {
						anchors.fill: parent;
						onClicked: {
							console.log("Removing " + listModel.get(index).name);
							forkManager.removePhilosopher(listModel.get(index).pid)
						}

						onPressed: {
							parent.color = "pink";
						}

						onReleased: {
							parent.color = "red";
						}
					}
				}

				Text {
					anchors.centerIn: parent;
					font.pixelSize: 24;

					text: "ID: " + pid + "\n" + name + "\n" + (status == 1 ? "Thinking" : (status == 2 ? "Waiting" : (status == 3 ? "Eating" : "Unknown")));
				}

				color: (status == 1 ? "Green" : (status == 2 ? "yellow" : (status == 3 ? "orange" : "grey")));
			}

		}
	}
}
